FROM python:3.7.7-slim-stretch

RUN mkdir /app
WORKDIR /app

ADD . .

RUN echo "export FLASK_DEBUG=1; export FLASK_APP=/app/server.py; flask run --host=0.0.0.0 --port=5000" > /run.sh

RUN pip install -r requirements.txt

RUN apt-get update

RUN apt-get install -y wget

RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb

RUN apt install -y ./google-chrome-stable_current_amd64.deb

RUN wget https://chromedriver.storage.googleapis.com/90.0.4430.24/chromedriver_linux64.zip

RUN apt-get install unzip

RUN unzip chromedriver_linux64

RUN mv chromedriver /usr/bin/chromedriver

EXPOSE 5000

CMD ["sh", "/run.sh"]
