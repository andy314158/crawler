from config import api

import logging
import requests
import sys
import json
import urllib.parse
import urllib.request

logger = logging.getLogger(__name__)


class API(object):
    def __init__(self):
        self.fetch_new_token()

    def perform_request(self, url, method='GET', body=[], attempt=0):
        attempt = attempt + 1
        if attempt <= 3:
            try:
                if method == 'GET':
                    response = requests.get(url, headers=self.mount_headers())
                else:
                    response = requests.post(url, headers=self.mount_headers(), json=body)

                """
                If status is 403, it means a new token is needed
                but if that's the second attemp stops to avoid loop
                """
                if response.status_code == 403:
                    self.fetch_new_token()
                    # retry after getting new token
                    return self.perform_request(url=url, attempt=attempt, method=method, body=body)
                else:
                    # 404 is how the API says "no result", not necessarily a problem
                    if response.status_code != 404:
                        response.raise_for_status()

                    # 201, 202, 203s don't have body so it fails to get the json...
                    if response.status_code == 200:
                        return response.json()

                    return response
            except:
                logger.error(sys.exc_info()[0])
                return self.perform_request(url=url, attempt=attempt, method=method, body=body)
        else:
            raise Exception('Could not establish connection to API after {} attempts'.format(attempt))


    def fetch_new_token(self, is_retry=0):
        # @TODO: CACHE TOKEN!
        url = ('{}/auth/login').format(api['HOST'])

        body = {
          "email": api['EMAIL'],
          "password": api['PASSWORD'],
        }

        try:
            response = requests.post(url, json=body)

            print(response.json())

            self.token = response.json()['access_token']

            # if fails and is a retry
            if response.status_code != 200 and is_retry <= 1:
                is_retry = is_retry + 1
                self.fetch_new_token(is_retry)

            response.raise_for_status()

        except requests.exceptions.HTTPError:
            raise Exception('Could not fetch token from API!')

    def mount_headers(self):
        return {
            'Authorization': 'Bearer {}'.format(self.token),
            'Content-Type': 'application/json'
        }

    def get_activity_provider(self, activity_provider_id):
        self.fetch_new_token()
        
        api_url = ('{}/activity_providers/{}').format(
            api['HOST'],
            activity_provider_id,
        )

        return self.perform_request(api_url)

    def get_activity_providers_with_active_tripadvisor_links(self):
        self.fetch_new_token()

        api_url = ('{}/tripadvisor/tripadvisor-active-links').format(
            api['HOST'],
        )
        response = self.perform_request(api_url)

        return response

    def save_tripadvisor_reviews_for_ap(self, reviews, activity_provider_id, url):
        self.fetch_new_token()

        api_url = ('{}/tripadvisor/save-tripadvisor-reviews').format(
            api['HOST']
        )

        # data to be sent to api
        body = { 
            "activity_provider_id" : activity_provider_id,
            "url" : url,
            "data": reviews
        }

        response = self.perform_request(api_url, 'POST', body)

        print('Process Done. Response: ' + response)

