from werkzeug.wrappers import Request, Response

import json, requests

class SNSHeaderValidator:
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        request = Request(environ)

        if request.path not in self.include():
            return self.app(environ, start_response)

        hdr = request.headers.get('X-Amz-Sns-Message-Type')

        # subscribe to the SNS topic
        if hdr == 'SubscriptionConfirmation':
            js = json.loads(request.data)
            subscribe_url = js['SubscribeURL']
            requests.get(subscribe_url)

            res = Response('{"success": true}', mimetype='application/json', status=200)
            return res(environ, start_response)

        return self.app(environ, start_response)

    def include(self):
        return [
            '/messages/refresh-snippet',
            '/notifications/process'
        ]