#!/usr/bin/env python
from flask import (Flask, jsonify, request, abort, render_template, logging)
from app.models.tripadvisor_attractions_scraper import TripadvisorAttractionsScraper
from app.models.tripadvisor_hotels_scraper import TripadvisorHotelsScraper
from app.models.tripadvisor_attractions_test import TripadvisorAttractionsTest
from app.models.tripadvisor_hotels_test import TripadvisorHotelsTest
from selenium.common.exceptions import NoSuchElementException

from app.services.api import API
from flask import Blueprint, jsonify, request
from flask_jwt import jwt_required

import json
import re
import logging
import sys

logger = logging.getLogger(__name__)

controller = Blueprint('tripadvisor', __name__)


@controller.route('/scrapeTripAdvisor', methods=['POST'])
@jwt_required()
def scrape_tripadvisor():
    api = API()
    if not request.json:
        abort(400)

    reviews = []
    url = request.json['url']
    min_date = request.json['min_date']

    if 'Hotel_Review' in url:
        tripadvisorScraper = TripadvisorHotelsScraper(min_date, url)
    else:
        tripadvisorScraper = TripadvisorAttractionsScraper(min_date, url)
    try:
        reviews = tripadvisorScraper.scrape_reviews()
        return (jsonify({'answer': reviews}), 200)
    except:
        return (jsonify({'answer': 'failed'}), 400)


@controller.route('/testTripadvisorLink', methods=['POST'])
@jwt_required()
def test_tripadvisor_url():
    if not request.json:
        abort(400)
    try:
        url = request.json['url']

        min_date = request.json['min_date']

        reviews = []

        if 'Hotel_Review' in url:
            tripadvisorScraper = TripadvisorHotelsTest(min_date, url)
        else:
            tripadvisorScraper = TripadvisorAttractionsTest(min_date, url)
        
        reviews = tripadvisorScraper.scrape_reviews()
        return (jsonify({'answer': reviews}), 200)
    except:
        return (jsonify({'answer': 'failed'}), 400)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
