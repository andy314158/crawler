import json
import datetime
from scrapy import Spider
from scrapy.shell import inspect_response
from scrapy.http import Request, FormRequest
from scrapy.utils.response import open_in_browser


class BookmeSpider(Spider):
    name = 'bookme'
    allowed_domains = ['bookme.com.au']

    def __init__(self, min_date, url):
        self.min_date = min_date
        self.url = url

        self.review_total_pages = []
        self.start = 0
        self.end = 4

    def start_requests(self):
        yield Request(self.url,
                      callback=self.parse)

    def parse(self, response):
        # open_in_browser(response)
        # inspect_response(response, self)

        hotel_name = response.xpath('//div[@class="allProducts"]/a/text()').extract_first()
        if hotel_name:
            hotel_name = hotel_name.replace('More from ', '')
        else:
            hotel_name = ''

        product_name = response.xpath('//h1/text()').extract_first()

        total_reviews = response.xpath('//*[@class="miniCount"]/text()').extract_first()
        if total_reviews:
            total_reviews = total_reviews.strip()
        else:
            total_reviews = ''

        total_rating = response.xpath('//span[@class="miniAvg"]/text()').extract_first()
        if total_rating:
            total_rating = total_rating.strip()
        else:
            total_rating = ''

        activity_id = response.url.split('/')[-1]

        reviews_url = 'https://www.bookme.com.au/things-to-do/json/reviews?activityId={}&rating=-1&start={}&end={}&language=EN&_=1621178052449'.format(activity_id, self.start, self.end)
        yield Request(reviews_url,
                      meta={'hotel_name': hotel_name,
                            'product_name': product_name,
                            'total_reviews': total_reviews,
                            'total_rating': total_rating,
                            'activity_id': activity_id},
                      callback=self.parse_reviews)

    def parse_reviews(self, response):
        # open_in_browser(response)
        # inspect_response(response, self)

        if 'Invalid end id' in response.body.decode('utf-8') or response.body == b'':
            yield {'hotel_name': response.meta['hotel_name'],
                   'product_name': response.meta['product_name'],
                   'total_reviews': response.meta['total_reviews'],
                   'total_rating': response.meta['total_rating'],
                   'language': '',
                   'ranking': '',
                   'location': '',
                   'reviews': self.review_total_pages}
        else:
            jsonresponse = json.loads(response.body.decode('utf-8'))
            if jsonresponse:
                for review_node in jsonresponse.get('reviews'):
                    review_text = review_node.get('comment')
                    review_date = review_node.get('date')
                    review_rating = review_node.get('perc')
                    review_author = review_node.get('name')

                    if self.min_date:
                        min_date_dt = datetime.datetime.strptime(self.min_date, '%b %Y')

                        review_date_dt = datetime.datetime.strptime(review_date, '%d %B %Y')

                        if min_date_dt > review_date_dt:
                            pass
                        else:
                            review_dict = {'review_text': review_text,
                                           'review_date': review_date,
                                           'review_header': '',
                                           'review_rating': review_rating,
                                           'review_author': review_author,
                                           'review_reply': ''}

                            self.review_total_pages.append(review_dict)

                self.start = self.start + 5
                self.end = self.end + 5
                reviews_url = 'https://www.bookme.com.au/things-to-do/json/reviews?activityId={}&rating=-1&start={}&end={}&language=EN&_=1621178052449'.format(response.meta['activity_id'], self.start, self.end)
                yield Request(reviews_url,
                              meta={'hotel_name': response.meta['hotel_name'],
                                    'product_name': response.meta['product_name'],
                                    'total_reviews': response.meta['total_reviews'],
                                    'total_rating': response.meta['total_rating'],
                                    'activity_id': response.meta['activity_id']},
                              callback=self.parse_reviews)
            else:
                yield {'hotel_name': response.meta['hotel_name'],
                       'product_name': response.meta['product_name'],
                       'total_reviews': response.meta['total_reviews'],
                       'total_rating': response.meta['total_rating'],
                       'language': '',
                       'ranking': '',
                       'location': '',
                       'reviews': self.review_total_pages}
