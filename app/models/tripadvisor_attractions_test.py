import sys
from json import dump
from time import sleep
from pyvirtualdisplay import Display

import argparse
import datetime
from parsel import Selector
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains

class TripadvisorAttractionsTestV2:
    
    def __init__(self, min_date=None, url=None):
        self.url = url
        self.min_date = min_date
 
    def get_all_reviews(self, hotel, language):
        review_total_pages = []
        
        user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36"

        options = webdriver.ChromeOptions()
        options.add_argument('--no-sandbox')
        options.headless = True
        options.add_argument('--headless')
        options.add_argument(f'user-agent={user_agent}')
        options.add_argument("--window-size=1920,1080")
        options.add_argument('--ignore-certificate-errors')
        options.add_argument('--allow-running-insecure-content')
        options.add_argument("--disable-extensions")
        options.add_argument("--proxy-server='direct://'")
        options.add_argument("--proxy-bypass-list=*")
        options.add_argument("--start-maximized")
        options.add_argument('--disable-gpu')
        options.add_argument('--disable-dev-shm-usage')
        page = webdriver.Chrome(chrome_options=options)

        page.get(hotel)
        sleep(2)

        # Get data
        hotel_name = page.find_element_by_xpath('//meta[@property="og:title"]').get_attribute('content').split(' - ')[0]

        total_reviews = page.find_element_by_xpath('//a[@href="#REVIEWS"]').text

        total_rating = page.find_element_by_xpath('//div[contains(@aria-label, "of 5 bubbles. ")]').get_attribute('aria-label')
        if total_rating:
            total_rating = total_rating.split()[0]
        else:
            total_rating = 50

        total_rating = int(total_rating)
        if total_rating > 5 :
            total_rating = total_rating / 10
        sleep(1)

        sel = Selector(text=page.page_source)

        json_node = sel.xpath('//footer/following::script/text()').extract_first()

        ranking = sel.xpath('//div[@data-automation="AppPresentation_PoiOverviewWeb"]//div/text()').extract_first()
        if ranking:
            ranking = ''.join(ranking)
        else:
            ranking = ''

        location = page.find_element_by_xpath('//div[text()="Address"]/following::button[1]/span').text

        page.quit()

        data = {'hotel_name': hotel_name,
                'total_reviews': total_reviews,
                'total_rating': total_rating,
                'ranking': ranking,
                'location': location}

        return data


    def scrape_reviews(self):
        hotels = [self.url]
        language = 'en'
        response = ''
        for hotel in hotels:
            print(f"IN PROCESS FOR: {hotel}")
            response = self.get_all_reviews(hotel, language)

        return response