import sys
from json import dump
from time import sleep
from pyvirtualdisplay import Display

import argparse
import datetime
from parsel import Selector
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains

class TripadvisorHotelsScraper:

    def __init__(self, min_date=None, url=None):
        self.url = url
        self.min_date = min_date

    def get_review_info(self, html_source, review_total_pages):
        sel = Selector(text=html_source)
        reviews = sel.xpath('//div[@data-test-target="HR_CC_CARD"]')
        for review in reviews:
            review_text = review.xpath('.//q//text()').extract()
            if review_text:
                review_text = [rt.replace('Read more', '') for rt in review_text]
                review_text = [rt for rt in review_text if rt != '']
                review_text = '\n'.join(review_text)
            else:
                review_text = ''

            review_date = review.xpath(
                './/*[contains(text(), "wrote a review ")]/text()').extract_first()
            if review_date:
                review_date = review_date.replace(' wrote a review ', '')

            review_header = review.xpath(
                './/div[@data-test-target="review-title"]//span/text()').extract_first()

            review_rating = review.xpath(
                './/span[contains(@class, "ui_bubble_rating bubble_")]/@class').extract_first()
            if review_rating:
                review_rating = review_rating.replace('ui_bubble_rating bubble_', '')
            else:
                review_rating = ''

            review_author = review.xpath(
                './/a[contains(@href, "/Profile/")]/text()').extract_first()

            review_reply = review.xpath('.//div[contains(text(), "Response from ")]/following-sibling::div//text()').extract()
            if review_reply:
                review_reply = [rp.replace('Read more', '') for rp in review_reply]
                review_reply = [rp for rp in review_reply if rp != '']
                review_reply = '\n'.join(review_reply)
            else:
                review_reply = ''
                if self.min_date:
                    min_date_dt = datetime.datetime.strptime(self.min_date, '%b %Y')
                
                formatted_review_date = review_date
                
                if self.validate(review_date) != True:
                    formatted_review_date = "May 2021"
                
                review_date_dt = datetime.datetime.strptime(formatted_review_date, '%b %Y')

                if min_date_dt > review_date_dt:
                    sys.exit()

            review_dict = { 'review_text': review_text,
                            'review_date': review_date,
                            'review_header': review_header,
                            'review_rating': review_rating,
                            'review_author': review_author,
                            'review_reply': review_reply}

            review_total_pages.append(review_dict)

    def validate(self, date_text):
        format = "%b %Y"

        try:
            datetime.datetime.strptime(date_text, format)
            return True
        except ValueError:
            return False

    def get_all_reviews(self, hotel, language):
        review_total_pages = []
        user_agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36"

        options = webdriver.ChromeOptions()
        options.add_argument('--no-sandbox')
        options.headless = True
        options.add_argument('--headless')
        options.add_argument(f'user-agent={user_agent}')
        options.add_argument("--window-size=1920,1080")
        options.add_argument('--ignore-certificate-errors')
        options.add_argument('--allow-running-insecure-content')
        options.add_argument("--disable-extensions")
        options.add_argument("--proxy-server='direct://'")
        options.add_argument("--proxy-bypass-list=*")
        options.add_argument("--start-maximized")
        options.add_argument('--disable-gpu')
        options.add_argument('--disable-dev-shm-usage')
        page = webdriver.Chrome(chrome_options=options)
        page.get(hotel)
        sleep(2)

        # Get data
        hotel_name = page.find_element_by_xpath('//h1[@id="HEADING"]').text

        total_reviews = page.find_element_by_xpath('//a[@href="#REVIEWS"]/span/following-sibling::span').text

        total_rating = page.find_element_by_xpath('//a[@href="#REVIEWS"]/span[contains(@class, "ui_bubble_rating bubble_")]').get_attribute('class')
        if total_rating:
            total_rating = total_rating.replace('ui_bubble_rating bubble_', '')
        else:
            total_rating = ''

        ranking = Selector(text=page.page_source).xpath('//b[@class="rank"]/parent::span//text()').extract()
        if ranking:
            ranking = ''.join(ranking)
        else:
            ranking = ''

        location = page.find_element_by_xpath('//span[contains(@class, "ui_icon map-pin-fill")]//following-sibling::span').text

        url_remove_http = hotel.replace('https://www.tripadvisor.co', '')
        url_split = url_remove_http.split('-Reviews-')

        self.get_review_info(page.page_source, review_total_pages)

        while True:
            try:
                # click on the next page button
                page.find_element_by_xpath('//a[@class="ui_button nav next primary "]').click()

                # wait for element to appear, then hover it
                wait = WebDriverWait(page, 10)
                men_menu = wait.until(ec.visibility_of_element_located((By.XPATH, '//div[@data-test-target="reviews-tab"]')))
                ActionChains(page).move_to_element(men_menu).perform()
                sleep(1.5)

                # click on Read more button to load all review text
                for page_btn in page.find_elements_by_xpath('//div[@data-test-target="expand-review"]/span[text()="Read more"]'):
                    try:
                        page_btn.click()
                        sleep(0.5)
                    except:
                        pass

                self.get_review_info(page.page_source, review_total_pages)

            except:
                break

        page.quit()

        data = {'hotel_name': hotel_name,
                'total_reviews': total_reviews,
                'total_rating': total_rating,
                'language': language,
                'reviews': review_total_pages,
                'ranking': ranking,
                'location': location}

        return data

    def scrape_reviews(self):
        hotels = [self.url]
        language = 'en'
        response = ''
        for hotel in hotels:
            print(f"IN PROCESS FOR: {hotel}")
            response = self.get_all_reviews(hotel, language)
        return response
