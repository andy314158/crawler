import sys
import json
import requests
from json import dump
from time import sleep
import argparse
import datetime
from parsel import Selector
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from webdriver_manager.chrome import ChromeDriverManager

driver = webdriver.Chrome(ChromeDriverManager().install())
# driver = webdriver.Remote("http://webbrowser:4444/wd/hub", DesiredCapabilities.CHROME)

class TripadvisorAttractionsScraperV3:
    
    def __init__(self, min_date=None, url=None):
        self.url = url
        self.min_date = min_date

    def get_review_info(self, html_source, review_total_pages):
        sel = Selector(text=html_source)
        review_urls = sel.xpath('//a[contains(@href, "/ShowUserReviews-")]/@href').extract()
        review_urls = ['https://www.tripadvisor.co.nz/' + review_url for review_url in review_urls]

        for review_url in review_urls:
            self.page = requests.get(review_url, headers={'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36'})
            sel = Selector(text=self.page.text)

            review_text = sel.xpath('//span[@class="fullText "]/text()').extract_first()
            if review_text:
                pass
            else:
                review_text = ''

            review_date = sel.xpath('//span[@class="ratingDate"]/@title').extract_first()

            review_header = sel.xpath('//h1[@class="title"]/text()').extract_first()

            review_rating = '10'
            try:
                try:
                    review_rating = review.xpath(
                        './/span[contains(@class, "ui_bubble_rating bubble_")]/@class').extract_first()
                    if review_rating:
                        review_rating = review_rating.replace('ui_bubble_rating bubble_', '')
                    else:
                        review_rating = ''
                except:
                    review_rating = sel.xpath(
                        '//div[@class="meta_inner"]//span[contains(@class, "ui_bubble_rating bubble_")]/@class').extract_first()
                    if review_rating:
                        review_rating = review_rating.replace('ui_bubble_rating bubble_', '')
                    else:
                        review_rating = '10'
                    pass
            except:
                review_rating = '10'
                pass

            review_author = sel.xpath('//div[@class="info_text"]/div/text()').extract_first()

            review_reply = sel.xpath('//*[@data-hiddenonpageload="true"]//p[@class="partial_entry"]//text()').extract()
            if review_reply:
                review_reply = '\n'.join(review_reply)
            else:
                review_reply = ''

            if self.min_date:
                min_date_dt = datetime.datetime.strptime(self.min_date, '%b %Y')
                review_date_dt = datetime.datetime.strptime(review_date, '%d %B %Y')

                if min_date_dt > review_date_dt:
                    sys.exit()

            review_dict = {'review_text': review_text,
                       'review_date': review_date,
                       'review_header': review_header,
                       'review_rating': review_rating,
                       'review_author': review_author,
                       'review_reply': review_reply}

            review_total_pages.append(review_dict)


    def get_all_reviews(self, hotel, language):
        review_total_pages = []
        
        user_agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36"
      
        self.page = driver

        self.page.maximize_window()

        self.page.get(hotel)
        sleep(2)

        # Get data
        hotel_name = self.page.find_element_by_xpath('//meta[@property="og:title"]').get_attribute('content').split(' - ')[0]

        total_reviews = self.page.find_element_by_xpath('//a[@href="#REVIEWS"]').text

        try:
            total_rating = page.find_element_by_xpath('//div[@class="ui_poi_review_rating  "]/span[contains(@class, "ui_bubble_rating bubble_")]').get_attribute('class')
            if total_rating:
                total_rating = total_rating.replace('ui_bubble_rating bubble_', '')
            else:
                total_rating = 50
        except:
            total_rating = 50
            pass

        # click to More button to load all attractions
        # self.page.find_element_by_xpath('//span[text()="Read more"]').click()
        sleep(1)

        attractions = Selector(text=self.page.page_source).xpath('//div[contains(@class, " hasAvatar")]//a[contains(@href, "/Attractions")]/text()').extract()

        sel = Selector(text=self.page.page_source)

        json_node = sel.xpath('//footer/following::script/text()').extract_first()

        ranking = sel.xpath('//div[@data-automation="AppPresentation_PoiOverviewWeb"]//div/text()').extract_first()
        if ranking:
            ranking = ''.join(ranking)
        else:
            ranking = ''

        try:
            try:
                location = self.page.find_element_by_xpath('//div[text()="Address"]/following::button[1]/span').text
            except: 
                location = page.find_element_by_xpath('//span[contains(@class, "ui_icon map-pin _")]/following-sibling::span').text
                pass
        except:
            location = 'not found'
            pass

        url_remove_http = hotel.replace('https://www.tripadvisor.co', '')
        url_split = url_remove_http.split('-Reviews-')

        self.get_review_info(self.page.page_source, review_total_pages)

        while True:
            try:
                # click on the next page button
                self.page.find_element_by_xpath('//a[@aria-label="Next page"]').click()
                sleep(2)

                # wait for element to appear, then hover it
                wait = WebDriverWait(self.page, 10)
                men_menu = wait.until(ec.visibility_of_element_located((By.XPATH, '//section[@id="REVIEWS"]')))
                ActionChains(self.page).move_to_element(men_menu).perform()
                sleep(1.5)

                self.get_review_info(self.page.page_source, review_total_pages)

            except:
                break

        self.page.quit()

        data = {'hotel_name': hotel_name,
                'total_reviews': total_reviews,
                'total_rating': total_rating,
                'language': language,
                # 'attractions': attractions,
                'reviews': review_total_pages,
                'ranking': ranking,
                'location': location}

        return data

    def scrape_reviews(self):
        hotels = [self.url]
        language = 'en'
        response = ''
        for hotel in hotels:
            print(f"IN PROCESS FOR: {hotel}")
            response = self.get_all_reviews(hotel, language)

        return response