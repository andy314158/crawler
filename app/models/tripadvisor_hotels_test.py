import sys
from json import dump
from time import sleep
from pyvirtualdisplay import Display

import argparse
import datetime
from parsel import Selector
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.action_chains import ActionChains

class TripadvisorHotelsTest:

    def __init__(self, min_date=None, url=None):
        self.url = url
        self.min_date = min_date

    def get_all_reviews(self, hotel, language):
        review_total_pages = []
        user_agent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36"

        options = webdriver.ChromeOptions()
        options.add_argument('--no-sandbox')
        options.headless = True
        options.add_argument('--headless')
        options.add_argument(f'user-agent={user_agent}')
        options.add_argument("--window-size=1920,1080")
        options.add_argument('--ignore-certificate-errors')
        options.add_argument('--allow-running-insecure-content')
        options.add_argument("--disable-extensions")
        options.add_argument("--proxy-server='direct://'")
        options.add_argument("--proxy-bypass-list=*")
        options.add_argument("--start-maximized")
        options.add_argument('--disable-gpu')
        options.add_argument('--disable-dev-shm-usage')
        page = webdriver.Chrome(chrome_options=options)
        page.get(hotel)
        sleep(2)

        # Get data
        hotel_name = page.find_element_by_xpath('//h1[@id="HEADING"]').text

        total_reviews = page.find_element_by_xpath('//a[@href="#REVIEWS"]/span/following-sibling::span').text

        total_rating = page.find_element_by_xpath('//a[@href="#REVIEWS"]/span[contains(@class, "ui_bubble_rating bubble_")]').get_attribute('class')
        if total_rating:
            total_rating = total_rating.replace('ui_bubble_rating bubble_', '')
        else:
            total_rating = ''

        ranking = Selector(text=page.page_source).xpath('//b[@class="rank"]/parent::span//text()').extract()
        if ranking:
            ranking = ''.join(ranking)
        else:
            ranking = ''

        location = page.find_element_by_xpath('//span[contains(@class, "ui_icon map-pin-fill")]//following-sibling::span').text

        url_remove_http = hotel.replace('https://www.tripadvisor.co', '')
        url_split = url_remove_http.split('-Reviews-')

        page.quit()

        data = {'hotel_name': hotel_name,
                'total_reviews': total_reviews,
                'total_rating': total_rating,
                'language': language,
                'reviews': review_total_pages,
                'ranking': ranking,
                'location': location}

        return data

    def scrape_reviews(self):
        hotels = [self.url]
        language = 'en'
        response = ''
        for hotel in hotels:
            print(f"IN PROCESS FOR: {hotel}")
            response = self.get_all_reviews(hotel, language)
        return response
