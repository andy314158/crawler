import re
import json
import datetime
from scrapy import Spider
from scrapy.shell import inspect_response
from scrapy.http import Request, FormRequest
from scrapy.utils.response import open_in_browser


class GetYourGuide(Spider):
    name = 'getyourguide'
    allowed_domains = ['getyourguide.com']

    def __init__(self, min_date, url):
    
        self.min_date = min_date
        self.url = url

    def start_requests(self,):
        yield Request(self.url,
                      callback=self.parse)

    def parse(self, response):
        hotel_name = response.xpath('//a[@class="supplier__details-location"]/text()').extract_first()

        product_name = response.xpath('//h1[@data-track="activity-title"]/text()').extract_first()

        total_reviews = response.xpath('//span[@class="gtm-trigger__adp-total-reviews-btn"]/text()').extract_first()
        if total_reviews:
            total_reviews = total_reviews.strip()
        else:
            total_reviews = ''

        total_rating = response.xpath('//p[@class="activity__rating--totals"]/text()').extract_first()
        if total_rating:
            total_rating = total_rating.strip()
            total_rating = total_rating.split('/')[0].strip()
        else:
            total_rating = ''

        reviews_summary = []
        reviews_summary_sel = response.xpath('//ul[@class="reviews-summary__categories-items reviews-summary__categories--content"]/li')
        for review_summary_sel in reviews_summary_sel:
            key = review_summary_sel.xpath('.//*[@class="reviews-summary__category-label"]/text()').extract_first()
            if key:
                key = key.strip()
            else:
                key = ''

            value = review_summary_sel.xpath('.//*[@class="reviews-summary__category-average"]/text()').extract_first()
            if value:
                value = value.strip()
            else:
                value = ''

            reviews_summary.append({key: value})

        location_id = re.findall('"sku":"(.*?)"', response.body.decode('utf-8'))[0]

        reviews_url = 'https://travelers-api.getyourguide.com/activities/{}/reviews?limit=1000&offset=0&sort=date:desc'.format(location_id)
        yield Request(reviews_url,
                      meta={'hotel_name': hotel_name,
                            'product_name': product_name,
                            'total_reviews': total_reviews,
                            'total_rating': total_rating,
                            'reviews_summary': reviews_summary},
                      callback=self.parse_reviews)

    def parse_reviews(self, response):
        # open_in_browser(response)
        # inspect_response(response, self)

        review_total_pages = []
        jsonresponse = json.loads(response.body.decode('utf-8'))
        for review_node in jsonresponse.get('reviews'):
            review_text = review_node.get('message')
            review_date = review_node.get('created')
            review_rating = review_node.get('rating')
            review_author = review_node.get('author').get('fullName')

            if self.min_date:
                min_date_dt = datetime.datetime.strptime(self.min_date, '%b %Y')

                if '+' in review_date:
                    review_date = review_date.split('+')[0]

                review_date_dt = datetime.datetime.strptime(review_date, '%Y-%m-%dT%H:%M:%S')

                if min_date_dt > review_date_dt:
                    pass
                else:
                    review_dict = {'review_text': review_text,
                                   'review_date': review_date,
                                   'review_header': '',
                                   'review_rating': review_rating,
                                   'review_author': review_author,
                                   'review_reply': ''}

                    review_total_pages.append(review_dict)

        yield {'hotel_name': response.meta['hotel_name'],
               'product_name': response.meta['product_name'],
               'total_reviews': response.meta['total_reviews'],
               'total_rating': response.meta['total_rating'],
               'language': '',
               'ranking': '',
               'location': '',
               'reviews_summary': response.meta['reviews_summary'],
               'reviews': review_total_pages}
