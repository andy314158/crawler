from werkzeug.security import safe_str_cmp


class User(object):
    def __init__(self, id=None, username=None, password=None):
        self.id = id
        self.username = username
        self.password = password

    def __str__(self):
        return "User(id='%s')" % self.id

    """
    @TODO Private repository, no biggy atm but probably best to move this somewhere safer
    """
    def get_users(self):
        users = [
            User(1, 'broadcaster', 'Pr0T3cTeDP@ssWord'),
            User(2, 'api', 'L3tG3tL0gg3d1nPl34s3'),
            User(3, 'facebook', 'R0D_5TW4RT'),
            User(4, 'payments', '@rct1c_M0nK3Ys2019')
        ]

        return users

    def get_username_table(self):
        users = self.get_users()

        return {u.username: u for u in users}

    def get_userid_table(self):
        users = self.get_users()

        return {u.id: u for u in users}

    def authenticate(self, username, password):
        username_table = self.get_username_table()

        user = username_table.get(username, None)
        if user and safe_str_cmp(user.password.encode('utf-8'), password.encode('utf-8')):
            return user

    def identity(self, payload):
        user_id = payload['identity']
        userid_table = self.get_userid_table()

        return userid_table.get(user_id, None)
