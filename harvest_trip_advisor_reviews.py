from app.services.api import API
from app.models.tripadvisor_attractions_scraper import TripadvisorAttractionsScraper
from app.models.tripadvisor_hotels_scraper import TripadvisorHotelsScraper
from app.models.tripadvisor_attractions_scraper_v2 import TripadvisorAttractionsScraperV2
from app.models.tripadvisor_attractions_test_v2 import TripadvisorAttractionsTestV2
from selenium.common.exceptions import NoSuchElementException

import datetime
import logging


logger = logging.getLogger(__name__)



def scrape_tripadvisor_reviews(url, activity_provider_id, min_date):
    try:
        api = API()
        data = []
        reviews = []
        url = request.json['url']
        min_date = request.json['min_date']

        if 'Hotel_Review' in url:
            tripadvisorScraper = TripadvisorHotelsScraper(min_date, url)
        elif 'tripadvisor' in url:
            tripadvisorScraper = TripadvisorAttractionsScraperV2(min_date, url)
        reviews = tripadvisorScraper.scrape_reviews()
        api.save_tripadvisor_reviews_for_ap(reviews, activity_provider_id, url)
    except KeyError:
        return

if __name__ == "__main__":
    api = API()

    active_tripadvisor_links = api.get_activity_providers_with_active_tripadvisor_links()

    # one_year_ago = datetime.datetime.now() - datetime.timedelta(days=365)
    # min_date = datetime.datetime.strptime(one_year_ago, '%b %Y')

    min_date = 'May 2021'

    for tripadvisor_link in active_tripadvisor_links:
        try:
            scrape_tripadvisor_reviews(tripadvisor_link['url'], tripadvisor_link['activity_provider_id'], min_date)
        except:
            continue