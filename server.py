
from app.models.user import User
from app.controllers.tripadvisor import controller as tripadvisor_controller
from flask import Flask, jsonify
from flask_jwt import JWT
from app.middlewares.sns_header_validator import SNSHeaderValidator

from config import jwt, sentry
from sentry_sdk.integrations.flask import FlaskIntegration

import sentry_sdk
import logging


app = Flask(__name__)
app.debug = False

# error logging
sentry_sdk.init(
    sentry['DSN'],
    integrations=[FlaskIntegration()]
)
logger = logging.getLogger(__name__)


#@app.errorhandler(Exception)
#def all_exception_handler(error):
#   logger.error(error)
#   return jsonify({'error': 'There has been an internal error'}), 500


@app.route('/health-check', methods=['GET'])
def health_check():
    return jsonify({
        'success': 'ok'
    }), 200

# register routes
app.register_blueprint(tripadvisor_controller)


# middlewares
app.wsgi_app = SNSHeaderValidator(app.wsgi_app)


# authentication
app.config['SECRET_KEY'] = jwt['TOKEN']
user = User()
jwt = JWT(app, user.authenticate, user.identity)


@jwt.jwt_error_handler
def error_handler(error):
    return jsonify({'error': error.error}), error.status_code

if __name__ == '__main__':
    app.run()