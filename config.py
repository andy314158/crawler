import os

from dotenv import load_dotenv
from pathlib import Path


env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)

jwt = {
    'TOKEN': os.getenv('JWT_TOKEN')
}

api = {
    'HOST': os.getenv('API_HOST'),
    'EMAIL': os.getenv('API_EMAIL'),
    'PASSWORD': os.getenv('API_PASSWORD'),
    'WEBHOOK_HOST': os.getenv('WEBHOOK_HOST')
}

sentry = {
    'DSN': os.getenv('SENTRY_DSN')
}


